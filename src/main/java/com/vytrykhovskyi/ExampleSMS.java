package com.vytrykhovskyi;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

public class ExampleSMS {
    public static final String ACCOUNT_SID = "AC7e80abadf09c07221926cc27152406df";
    public static final String AUTH_TOKEN = "cbe3e43fdfcb980f31195487c1a8ee4c";

    public static void send(String str) {
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
        Message message = Message.creator(new PhoneNumber("+380676780828"),
                new PhoneNumber("+19046060743"), str).create();
    }
}
